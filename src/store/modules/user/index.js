import {loginData,safeUserData,safeOutLoginData,checkVCodeData,isRegData,regUserData,getUserInfoData,uploadHeadData,updataUserInfoData,
    updateCellphoneData,updatePasswordData,getFavData,delFavData} from "../../../api/user";
let modules={
    namespaced:true,
    state:{
        uid:localStorage['uid']?localStorage['uid']:"",
        nickname:localStorage['nickname']?localStorage['nickname']:"",
        isLogin:localStorage['isLogin']?Boolean(localStorage['isLogin']):false,
        authToken:localStorage["authToken"]?localStorage["authToken"]:"",
        head:"",
        points:0,
        favs:[],    //收藏的数据源
    },
    mutations:{
        ["SET_LOGIN"](state,payload){
            state.uid=payload.uid;
            state.nickname=payload.nickname;
            state.isLogin=payload.isLogin;
            state.authToken=payload.authToken;
            localStorage["uid"]=payload.uid;
            localStorage['nickname']=payload.nickname;
            localStorage['isLogin']=payload.isLogin;
            localStorage["authToken"]=payload.authToken;
        },
        ["OUT_LOGIN"](state){
            state.uid="";
            state.nickname="";
            state.isLogin=false;
            state.authToken="";
            state.points = 0;
            state.head = '';
            localStorage.removeItem("uid");
            localStorage.removeItem("nickname");
            localStorage.removeItem("isLogin");
            localStorage.removeItem("authToken");
            localStorage.removeItem("cartData");    //退出的时候清空购物车数据
            sessionStorage.removeItem("addsid");    //把临时存储的购物车数据也进行清空，为了安全起见
        },
        ['SET_USER_INFO'](state,payload) {
            state.head = payload.head;
            state.points = payload.points;
            state.nickname = payload.nickname;
        },
        //设置我的收藏
        ["SET_FAVS"](state,payload){
            state.favs = payload.favs;
        },
        //设置我的收藏分页
        ["SET_PAVS_PAGE"](state,payload){
            state.favs.push(...payload.favs);
        },
        //删除收藏
        ["DEL_FAVS"](state,payload){
            state.favs.splice(payload.index,1);
        }
    },
    actions:{
        //会员登录
        login(conText,payload){
            loginData(payload).then(res=>{
                // console.log(res);
                if (res.code===200){
                    conText.commit("SET_LOGIN",{uid:res.data.uid,nickname:res.data.nickname,isLogin:true,authToken:res.data.auth_token});
                }
                if (payload.success) {
                    payload.success(res)
                }
            })
        },
        //安全退出
        outLogin(conText){
            safeOutLoginData({uid:conText.state.uid}).then(res=>{
                // console.log(res);
            });
            //当用户安全退出的时候，那么购物车中的数据应该也清空
            conText.rootState.cart.cartData = [];
            conText.commit("OUT_LOGIN");
        },
        //会员安全认证
        safeUser(conText,payload){
            // console.log(conText.state.uid);
            safeUserData({uid:conText.state.uid,auth_token:conText.state.authToken}).then(res=>{
                // console.log(res);
                if(res.code!==200){
                    conText.commit("OUT_LOGIN");
                    conText.rootState.cart.cartData = [];   //当遇到非法入侵的时候让购物车中的数据清空
                }
                if (payload.success){
                    payload.success(res)
                }
            });
        },
        //检测图片验证码
        checkVCode(conText,payload){
            return checkVCodeData(payload.vcode).then(res=>{
                return res;
            })
        },
        //是否注册会员
        isReg(conText,payload){
            return isRegData(payload.username).then(res=>{
                return res;
            })
        },
        //注册会员
        regUser (conText,payload) {
            regUserData(payload).then (res=>{
                if (payload.success) {
                    payload.success(res);
                }
            })
        },
        //获取会员信息
        getUserInfo(conText,payload){
            getUserInfoData(conText.state.uid).then(res=>{
                if (res.code === 200) {
                    conText.commit("SET_USER_INFO",{head:res.data.head,points:res.data.points,nickname:res.data.nickname});
                    //如果payload数据存在并且通过回调函数获取数据，那么把data数组中的数据传出去
                    if (payload && payload.success) {
                        payload.success(res.data);
                    }
                }
            })
        },
        //上传头像
        uploadHead(conText,payload){
            uploadHeadData(payload).then(res=>{
                // console.log(res);
                if (payload.success) {
                    payload.success(res);
                }
            })
        },
        //修改会员信息
        updateUserInfo(conText,payload){
            updataUserInfoData({uid:conText.state.uid,...payload}).then(res=>{
                // console.log(res);
                if (payload.success) {
                    payload.success(res);
                }
            })
        },
        //修改手机号
        updateCellphone(conText,payload) {
            updateCellphoneData({uid:conText.state.uid,...payload}).then(res=>{
                // console.log(res);
                if (payload.success) {
                    payload.success(res);
                }
            })
        },
        //修改密码
        updatePassword(conText,payload){
            updatePasswordData({uid:conText.state.uid,...payload}).then(res=>{
                // console.log(res);
                if (payload.success) {
                    payload.success(res);
                }
            })
        },
        //我的收藏
        getFav(conText,payload){
            getFavData({uid:conText.state.uid,...payload}).then(res=>{
                // console.log(res);
                let pageNum = 0;
                if (res.code === 200) {
                    conText.commit("SET_FAVS",{favs:res.data});
                    pageNum=res.pageinfo.pagenum;
                }else{
                    conText.commit("SET_FAVS",{favs:[]});
                    pageNum = 0;
                }
                if (payload.success) {
                    payload.success(pageNum);
                }
            })
        },
        //我的收藏分页
        getFavPage(conText,payload){
            getFavData({uid:conText.state.uid,...payload}).then(res=>{
                if (res.code === 200) {
                    conText.commit("SET_PAVS_PAGE",{favs:res.data});
                    if (payload.success) {
                        payload.success();
                    }
                }
            })
        },
        //删除收藏
        delFav(conText,payload){
            delFavData({uid:conText.state.uid,...payload}).then(res=>{
                // console.log(res);
                if (res.code === 200) {
                    conText.commit("DEL_FAVS",{index:payload.index})
                    if (payload.success) {
                        payload.success();
                    }
                }
            })
        }
    }
}
export default modules;