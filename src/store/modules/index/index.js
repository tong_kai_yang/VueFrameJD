//引入接口文件
import {getNavsData,getSwiperData,getGoodsData,getRecomGoodsData} from "../../../api/index";

export default {
    namespaced:true,
    state:{
        navs:[],
        swipers:[],
        goods:[],
        recom:[],
    },
    /*
    mutations，里面装着一些改变数据方法的集合，这是Vuex设计很重要的一点
        就是把处理数据逻辑方法全部放在mutations里面，使得数据和视图分离。
        不过就是有同步操作的限制，而actions没有这个限制，可以包含任何的异步操作，但是actions提交操作到mutations
        而不是直接改变状态，而mutations主要是改变state的值

        state:代表的是最初的值，而payload是改变之后的值
        ["SET_NAVS"]:代表的是表达式，属于ES6的新特性,代表一个常量
        conText：能拿到所有的值以及state中的数据
        conText.commit():如果进行报错了的话，包裹在字符串中即可，分电脑，有的电脑不会出现这个问题
        modules:模块
        namespaced:true 命名空间，防止命名重复，阻止一改都改的情况
        * as types:如果表达式过多的情况下，使用* as types来声明，之后[types."SET_NAVS"]这种形式进行调用,返回的是一个对象
        vuex状态管理如果进行F5刷新，页面会丢失，因为是临时存储，解决方式：localStorage
    */
    mutations:{
        ["SET_NAVS"](state,payload){
            state.navs=payload.navs;
        },
        ["SET_SWIPER"](state,payload){
            state.swipers = payload.swipers;
        },
        ["SET_GOODS"](state,payload) {
            state.goods = payload.goods;
        },
        ["SET_RECOM"](state,payload) {
            state.recom = payload.recom;
        }
    },
    //异步操作
    actions:{
        //首页快速导航
        getNavs(conText,payload){
            getNavsData().then(res=>{
                // console.log(res);
                if (res.code===200){
                    conText.commit("SET_NAVS",{navs:res.data});
                    if (payload.success) {
                        payload.success();
                    }
                }
            })
        },
        //此时的payload会把之前的回调函数给获取过来
        //首页轮播图
        getSwiper(conText,payload) {
            getSwiperData().then(res=>{
                if (res.code === 200) {
                    conText.commit("SET_SWIPER",{swipers:res.data});
                    /*
                    但还是不好使，那是因为里面需要获取DOM操作，而created是无法获取DOM操作的，所以需要使用$nextTick来解决
                    为什么不放在mounted中，是因为有数据流的存在，而数据流需要在created中设置，
                    为了严谨一些，进行一下判断，判断回调方法是否存在，如果存在正常执行，如果不存在给出提示
                    */
                   if (payload.success) {
                        payload.success()
                   }                   
                }
            })
        },
        //首页核心产品
        getGoods(conText,payload){
            getGoodsData().then(res=>{
                if (res.code === 200) {
                    conText.commit("SET_GOODS",{goods:res.data});
                    if (payload.success) {
                        payload.success();
                    }
                }
            })
        },
        //首页推荐
        getRecomGoods(conText,payload){
            getRecomGoodsData().then(res=>{
                if (res.code === 200) {
                    conText.commit("SET_RECOM",{recom:res.data});
                    if (payload.success) {
                        payload.success();
                    }
                }
            })
        }
    }
}