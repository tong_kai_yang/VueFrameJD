import {addOrderData,getOrderNumData,getMyOrderData,cancelOrderData,sureOrderData,getOrderInfoData,getReviewOrderData,getReviewServiceData,addReviewData} from "../../../api/order";

export default {
    namespaced:true,
    state:{
        orderNum:"",
        orders:[],  //把获取到的数据放到数组中
        orderInfo:{},
        reviewOrders:[],
        reviewServices:[],
    },
    mutations:{
        ["SET_ORDERNUM"](state,payload) {
            state.orderNum = payload.orderNum;
        },
        //我的订单
        ["SET_ORDERS"](state,payload){
            state.orders = payload.orders;
        },
        //我的订单分页
        ["SET_ORDERS_PAGE"](state,payload){
            state.orders.push(...payload.orders);   //把新得数据依次添加进之前的老数组中，让其进行渲染
        },
        //取消订单
        ['DEL_ORDERS'](state,payload){
            state.orders.splice(payload.index,1);
        },
        //改变订单状态
        ["SET_STATUS"](state,payload){
            state.orders[payload.index].status = payload.status;
        },
        //设置订单详情
        ["SET_ORDER_INFO"](state,payload) {
            state.orderInfo = payload.orderInfo;
            // console.log(state.orderInfo);
        },
        //设置待评价订单
        ["SET_REVIEW_ORDER"](state,payload) {
            state.reviewOrders = payload.reviewOrders;
        },
        //设置待评价订单分页
        ["SET_REVIEW_ORDERS_PAGE"](state,payload){
            //在此处添加扩展运算符添加进去，代替了for循环,把新得数据依次添加进之前的老数组中，让其进行渲染
            state.reviewOrders.push(...payload.reviewOrders);
        },
        //设置评价服务选项
        ["SET_REVIEW_SERVICES"](state,payload){
            state.reviewServices = payload.reviewServices;
        },
        //设置评价分数
        ["SET_REVIEW_SCORE"](state,payload){
            //判断数组中是否有数据，如果有进行循环
            if (state.reviewServices.length > 0) {
                //此处循环为了更好得性能优化，这个循环是外围最大得数组，但是不需要从头开始循环，取第二个数组中的下标+1，取后几位即可，这样比较的话始终会少一位，能加快加载速度
                for (let i = payload.index2+1; i < state.reviewServices[payload.index].scores.length;i++) {
                    state.reviewServices[payload.index].scores[i].active = false;
                }
                //此处循环的是第二个数组中的下标值，也就是星值
                for (let i = 0; i <= payload.index2; i++) {
                    //循环数组中的下标之后把星值得状态改变为true
                    state.reviewServices[payload.index].scores[i].active = true;
                }
                //把新得星值赋给之前得值，顶替掉
                state.reviewServices[payload.index].score = payload.score;
            }
        },
    },
    actions:{
        //提交订单
        addOrder(conText,payload){
            addOrderData({uid:conText.rootState.user.uid,...payload}).then(res=>{
                console.log(res);
                if (payload.success) {
                    payload.success(res);
                }
            }) 
        },
        //获取最后订单编号
        getOrderNum(conText,payload) {
            getOrderNumData(conText.rootState.user.uid).then(res=>{
                // console.log(res);
                if (res.code === 200) {
                    conText.commit("SET_ORDERNUM",{orderNum:res.data.ordernum});
                }
            })
        },
        //我的订单
        getMyOrder(conText,payload) {
            getMyOrderData({uid:conText.rootState.user.uid,...payload}).then(res=>{
                // console.log(res);
                //设置懒加载
                let pageNum = 0;
                if(res.code === 200){
                    pageNum = res.pageinfo.pagenum;
                    conText.commit("SET_ORDERS",{orders:res.data});
                }else{
                    pageNum = 0;
                    conText.commit("SET_ORDERS",{orders:[]});
                }
                if (payload.success) {
                    payload.success(pageNum);
                }
            })
        },
        //我的订单分页
        getMyOrderPage(conText,payload){ 
            getMyOrderData({uid:conText.rootState.user.uid,...payload}).then(res=>{
                if (res.code === 200) {
                    conText.commit("SET_ORDERS_PAGE",{orders:res.data});
                }
            })
        },
        //取消订单
        cancelOrder(conText,payload){
            cancelOrderData({uid:conText.rootState.user.uid,...payload}).then(res=>{
                // console.log(res)
                if (res.code === 200) {
                    conText.commit("DEL_ORDERS",{index:payload.index})
                }
            })
        },
        //确认订单
        sureOrder(conText,payload) {
            sureOrderData({uid:conText.rootState.user.uid,...payload}).then(res=>{
                // console.log(res);
                if (res.code === 200) {
                    conText.commit("SET_STATUS",{index:payload.index,status:payload.status});
                }
            })
        },
        //订单详情
        getOrderInfo(conText,payload){
            getOrderInfoData({uid:conText.rootState.user.uid,...payload}).then(res=>{
                // console.log(res);
                if (res.code === 200) {
                    conText.commit("SET_ORDER_INFO",{orderInfo:{ordernum:res.data.ordernum,name:res.data.name,cellphone:res.data.cellphone,status:res.data.status,
                    province:res.data.province,city:res.data.city,area:res.data.area,address:res.data.address,freight:res.data.freight,total:res.data.total,
                truetotal:res.data.truetotal,ordertime:res.data.ordertime,goods:res.data.goods}});
                }
            })
        },
        //待评价订单
        getReviewOrder(conText,payload) {
            getReviewOrderData({uid:conText.rootState.user.uid,...payload}).then(res=>{
                // console.log(res);
                //因为需要分页
                let pageNum = 0;
                if (res.code === 200) {
                    pageNum = res.pageinfo.pagenum;
                    conText.commit("SET_REVIEW_ORDER",{reviewOrders:res.data});
                }else{
                    pageNum = 0;
                    conText.commit("SET_REVIEW_ORDER",{reviewOrders:[]});
                }
                if (payload.success) {
                    payload.success(pageNum);
                }
            })
        },
        //待评价订单分页设置
        getReviewOrderPage(conText,payload) {
            getReviewOrderData({uid:conText.rootState.user.uid,...payload}).then(res=>{
                if (res.code === 200) {
                    conText.commit("SET_REVIEW_ORDERS_PAGE",{reviewOrders:res.data});
                }
            })
        },
        //评价服务选项
        getReviewService(conText) {
            getReviewServiceData().then(res=>{
                // console.log(res);
                if (res.code === 200) {
                    /*
                    因为需要设置星级服务，但是后台并没有数据，那么就需要自己创建一个传给后台，尤其是点亮星星的时候，循环后台数据中的数组
                    */ 
                   for (let i = 0; i < res.data.length;i++) {
                        res.data[i].score = 5;  //默认星级服务为0
                        res.data[i].scores = [
                            {
                                value:1,
                                active:true
                            },
                            {
                                value:2,
                                active:true
                            },
                            {
                                value:3,
                                active:true
                            },
                            {
                                value:4,
                                active:true
                            },
                            {
                                value:5,
                                active:true
                            },
                        ]
                   }
                    conText.commit("SET_REVIEW_SERVICES",{reviewServices:res.data})
                }
            })
        },
        //提交评价
        addReview(conText,payload){
            addReviewData({uid:conText.rootState.user.uid,...payload}).then(res=>{
                // console.log(res);
                if (payload.success) {
                    payload.success(res);
                }
            })
        }
    }
}