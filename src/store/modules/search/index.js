import {getHotKeywordData,getSearchData,getAttrsData} from '../../../api/search';
import Vue from 'vue';
export default {
    namespaced:true,
    state:{
        //如果存在走localStorage['historyKeywords']，如果不存在为空
        historyKeywords:localStorage['historyKeywords']?JSON.parse(localStorage['historyKeywords']):[],
        hotKeywords:[],
        priceData:{
            isHide:false,
            items:[
                {price1:1,price2:50,active:false},
                {price1:51,price2:99,active:false},
                {price1:100,price2:300,active:false},
                {price1:301,price2:1000,active:false},
                {price1:1001,price2:4000,active:false},
                {price1:4001,price2:9999,active:false},
            ]
        },
        minPrice:"",
        maxPrice:"",
        attrs:[],
        searchData:[],
        //此步骤是进行筛选操作
        cid:"",
        params:[],
        total:0,
    },
    mutations:{
        //设置历史记录关键词
        ["SET_KEYWORDS"](state,payload) {
            state.historyKeywords = payload.historyKeywords;
            /*
                虽然值渲染到了页面上，但是一刷新就没有了，所以需要存储起来，存到localStorage中，但是需要把数组中的对象转化为
                字符串，否则日后会有影响，但是在数据源中需要在把字符串转换为对象
            */
           localStorage['historyKeywords'] = JSON.stringify(state.historyKeywords)
        },
        //删除搜索存储的记录
        ["CLEAR_KEYWORDS"](state,payload) {
            state.historyKeywords=[];
            localStorage.removeItem("historyKeywords");
        },
        //设置热门关键词
        ["SET_HOTKEYWORD"](state,payload) {
            state.hotKeywords = payload.hotKeywords;
        },
        //隐藏价格
        ["HIDE_PRICE"](state,payload) {
            state.priceData.isHide = !state.priceData.isHide ;
        },
        //选择价格
        ["SELECT_PRICE"](state,payload) {
            if (state.priceData.items.length > 0) {
                for (let i = 0; i < state.priceData.items.length; i++) {
                    if (i !== payload.index) {
                        if (state.priceData.items[i].active) {
                            state.priceData.items[i].active=false;
                            break;
                        }
                    }
                }
                //选中的时候最大以及最小的区间会有值，但是如果选中之后再次取消的话，那么最大以及最小的区间值应该也消失掉
                state.priceData.items[payload.index].active=!state.priceData.items[payload.index].active;
                Vue.set(state.priceData.items,payload.index,state.priceData.items[payload.index])
                //进行判断，如果为true的话，那么执行选中效果，赋值，如果为false的话，那么取消执行选中效果，同时取消赋值
                state.minPrice =state.priceData.items[payload.index].active?state.priceData.items[payload.index].price1:"";
                state.maxPrice =state.priceData.items[payload.index].active?state.priceData.items[payload.index].price2:"";
            }
        },
        //设置最小价格,此处的正则表达式是排除掉字母
        ["SET_MINPRICE"](state,payload) {
            state.minPrice = payload.minPrice;
            state.minPrice = state.minPrice.replace(/[^\d|\.]/g,"");
        },
        //设置最大价格
        ["SET_MAXPRICE"](state,payload) {
            state.maxPrice = payload.maxPrice;
            state.maxPrice = state.maxPrice.replace(/[^\d|\.]/g,"");
        },
        //显示隐藏商品属性
        ['HIDE_ATTR'](state,payload) {
            state.attrs[payload.index].isHide = !state.attrs[payload.index].isHide;
            Vue.set(state.attrs,payload.index,state.attrs[payload.index]);
        },
        //选择商品属性
        ['SELECT_ATTR'] (state,payload) {
            state.attrs[payload.index].param[payload.index2].active = !state.attrs[payload.index].param[payload.index2].active;
            Vue.set(state.attrs[payload.index].param,payload.index2,state.attrs[payload.index].param[payload.index2]);
        },
        //设置搜索结果
        ["SET_SEARCH_DATA"](state,payload) {
            state.searchData = payload.searchData;
            state.total = payload.total;
        },
        //增加分页数据
        ["SET_SEARCH_DATA_PAGE"](state,payload){
            if (payload.searchData.length > 0) {
                for (let i = 0; i < payload.searchData.length;i++) {
                    state.searchData.push(payload.searchData[i]);
                }
            }
        },
        //设置商品分类的cid
        ["SET_CID"](state,payload) {
            state.cid = payload.cid;
        },
        ["SET_ATTRS"](state,payload) {
            state.attrs = payload.attrs;
        },
        //设置属性的值
        ["SET_PARAMS"](state,payload) {
            if (state.attrs.length > 0) {
                state.params = [];
                for (let i = 0;i < state.attrs.length; i++) {
                    for (let j = 0; j < state.attrs[i].param.length;j++) {
                        if (state.attrs[i].param[j].active) {
                            state.params.push(state.attrs[i].param[j].pid);
                        }
                    }
                }
            }
        },
        ["RESET_SCREEN"](state) {
            state.cid = "";
            //重置价格
            if (state.priceData.items.length > 0) {
                for (let i = 0; i < state.priceData.items.length; i++) {
                    if (state.priceData.items[i].active) {
                        state.priceData.items[i].active=false;
                        break;
                    }
                }
                state.minPrice = "";
                state.maxPrice = "";
            }
            //重置属性
            if (state.attrs.length > 0) {
                for (let i = 0;i < state.attrs.length; i++) {
                    for (let j = 0; j < state.attrs[i].param.length;j++) {
                        if (state.attrs[i].param[j].active) {
                            state.attrs[i].param[j].active=false;
                        }
                    }
                }
                state.params = [];
            }
        }
    },
    /*
        如果不涉及到api中的接口以及ajax交互的时候，可以不用actions，直接使用mutations就可以
    */
    actions:{
        getHotKeyword(conText,payload){
            getHotKeywordData().then(res=>{
                // console.log(res);
                if (res.code === 200) {
                    conText.commit("SET_HOTKEYWORD",{hotKeywords:res.data})
                }
            })
        },
        //选择分类
        selectClassify (conText,payload) {
            // console.log(conText);
            if (conText.rootState.goods.classifys.length > 0) {
                for (let i = 0; i < conText.rootState.goods.classifys.length;i++) {
                    //这是第一种方式：不触发当前的改变状态
                    // if (i !== payload.index) {
                    //     if (conText.rootState.goods.classifys[i].active) {
                    //         conText.rootState.goods.classifys[i].active = false;   
                    //         break;
                    //     }
                    // }

                    //第二种方式就是直接赋值，把原本的值直接赋过去
                    if (conText.rootState.goods.classifys[i].active) {
                        conText.rootState.goods.classifys[i].active = conText.rootState.goods.classifys[payload.index].active;
                        break;
                    }
                }
                conText.rootState.goods.classifys[payload.index].active = !conText.rootState.goods.classifys[payload.index].active;
                Vue.set(conText.rootState.goods,payload.index,conText.rootState.goods.classifys[payload.index]);

                //此处要判断一下，就是筛选的时候点击时候面板需要取消，但是cid也要消失，所以需要判断一下
                let cid = conText.rootState.goods.classifys[payload.index].active ? conText.rootState.goods.classifys[payload.index].cid:"";

                //conText.rootState.goods.classifys[payload.index]能获取每次点击的下标值,通过下标值确定当前的ID值
                conText.commit("SET_CID",{cid:cid});
            }
        },
        //获取商品搜索结果
        /*
            此处会有一个问题，就是当滑动加载到多内容的时候，如果这个时候筛选，那么没有的值，点击也会出现结果，这是一个bug，
            问题是出在就是为200的时候才执行，那么不为200的时候回调函数里的内容不执行了，那么判断一下，是不是200都执行，让
            其结果不同就好了
        */
        getSearch(conText,payload) {
            getSearchData(payload).then(res=>{
                let pageNum = 0;
                if (res.code === 200) {
                    pageNum = res.pageinfo.pageNum;
                    conText.commit("SET_SEARCH_DATA",{searchData:res.data,total:res.pageinfo.total});
                }else{
                    pageNum = 0;
                    conText.commit("SET_SEARCH_DATA",{searchData:[],total:0});
                }
                if (payload.success) {
                    payload.success(res);
                }
            }) 
        },
        //因为数据太多了，需要分页，但是需要把分页的数据给放到之前的数组中
        getSearchPage(conText,payload) {
            getSearchData(payload).then(res=>{
                if (res.code === 200) {
                    conText.commit("SET_SEARCH_DATA_PAGE",{searchData:res.data});
                }
            }) 
        },
        //获取商品属性
        getAttrs(conText,payload) {
            getAttrsData(payload.keyword).then(res=>{
                console.log(res);
                if (res.code === 200) {
                    for (let i = 0;i < res.data.length;i++) {
                        res.data[i].isHide = false;
                        for(let j = 0; j < res.data[i].param.length;j++){
                            res.data[i].param[j].active=false;
                        }
                    }
                    conText.commit("SET_ATTRS",{attrs:res.data})
                }else{
                    conText.commit("SET_ATTRS",{attrs:[]})
                }
                if (payload.success) {
                    payload.success();
                }
            })
        },
        //筛选面板重置
        resetScreen(conText){
           if (conText.rootState.goods.classifys.length > 0) {
               for (let i = 0; i < conText.rootState.goods.classifys.length;i++) {
                    if (conText.rootState.goods.classifys[i].active) {
                        conText.rootState.goods.classifys[i].active=false;
                        break;
                    }
               } 
           }
           conText.commit("RESET_SCREEN")
        }
    }
}