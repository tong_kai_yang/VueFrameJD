import Vue from "vue"
export default {
    namespaced:true,
    state:{
        //判断，如果存在那么正常渲染数据，如果不存在为空，但是传过来的是字符串，需要转换为对象
        cartData:localStorage['cartData'] ? JSON.parse(localStorage['cartData']):[]
    },
    mutations:{
        //添加购物车商品
        ["ADD_ITEM"](state,payload) {
            /*
            此处解决的是添加购物车的时候，如果有重复的应该直接添加个数，而不是直接把数据再次添加一次，所以需要去掉重复的
            解决方式：声明一个变量，给一个设定的值，之后进行判断，如果不等于设定的值，那么进行数据添加
            */
            let isSame = false;
            //为了谨慎，那么相同的话需要再次判断
            if (state.cartData.length > 0) {
                for (let i = 0; i < state.cartData.length; i++) {
                    //此处的问题就是不同规格的需要重新渲染数据，但是并没有，所以需要再次判断一下规格
                    if (state.cartData[i].gid === payload.cartData.gid && JSON.stringify(state.cartData[i].attrs)===JSON.stringify(payload.cartData.attrs)) {
                        isSame = true;
                        //虽然去掉了重复，但是数量并没有增加，传过来的是字符串需要给转换一下，小坑
                        state.cartData[i].amount = parseInt(state.cartData[i].amount)+parseInt(payload.cartData.amount);
                        break;
                    }
                }
            }
            //如果没有相同的，那就添加进去
            if (!isSame) {
                state.cartData.push(payload.cartData);
            }
            //此处的问题是页面刷新之后，数据就没有了，所以需要配合localStorage来解决，但是缓存的数据是对象，需要转换为字符串
            localStorage['cartData'] = JSON.stringify(state.cartData);
        },
        //删除商品
        ["DEL_ITEM"](state,payload){
            state.cartData.splice(payload.index,1);
            localStorage['cartData'] = JSON.stringify(state.cartData);
        },
        //更改数量
        ["SET_AMOUNT"] (state,payload) {
            state.cartData[payload.index].amount = payload.amount;
            state.cartData[payload.index].amount =  parseInt(state.cartData[payload.index].amount.replace(/[^\d]/g,""))
            if (!state.cartData[payload.index].amount) {
                state.cartData[payload.index].amount=1;
            }
            localStorage['cartData'] = JSON.stringify(state.cartData);
        },
        //增加数量
        ['INC_AMOUNT'](state,payload){
            state.cartData[payload.index].amount += 1;
            localStorage['cartData'] = JSON.stringify(state.cartData);
        },
        //减少数量
        ['DEC_AMOUNT'](state,payload){
            state.cartData[payload.index].amount = state.cartData[payload.index].amount > 1 ? --state.cartData[payload.index].amount:1;
            Vue.set(state.cartData,payload.index,state.cartData[payload.index])
            localStorage['cartData'] = JSON.stringify(state.cartData);
        },
        //选择商品
        ["SELECT_ITEM"](state,payload){
            state.cartData[payload.index].checked = !state.cartData[payload.index].checked
            Vue.set(state.cartData,payload.index,state.cartData[payload.index])
            localStorage['cartData'] = JSON.stringify(state.cartData);
        },
        //全选/反选
        ["ALL_SELECT_ITEM"](state,payload) {
            if (state.cartData.length > 0) {
                for (let i = 0; i < state.cartData.length; i++) {
                    state.cartData[i].checked = payload.checked;
                }
                localStorage['cartData'] = JSON.stringify(state.cartData);
            }
        }
    },
    //vuex中的计算操作
    getters:{
        //计算总金额
        total(state) {
            if (state.cartData.length > 0) {
                let total = 0;
                for (let i = 0; i < state.cartData.length; i++) {
                    if (state.cartData[i].checked) {
                        total+=state.cartData[i].price * state.cartData[i].amount;
                    }
                }
                return parseFloat(total.toFixed(2));  //记得要四舍五入，保留两位小数
            }else{
                return 0;
            }
        },
        //运费,先把运费存放到要给数组中，之后在获取其中得最大值，
        freight(state){
            if (state.cartData.length > 0) {
                let freights = [];
                for (let i = 0; i < state.cartData.length; i++) {
                    if (state.cartData[i].checked) {
                        freights.push(state.cartData[i].freight);
                    }
                }
                // console.log(freights)
                return freights.length>0?Math.max.apply(null,freights):0;   //获取最大值,但此时有一个问题就是说如果取消掉选择之后，运费会为异常，是因为运费数组为空所以导致，需要判断一下
            }else{
                return 0;
            }
        }
    }
}