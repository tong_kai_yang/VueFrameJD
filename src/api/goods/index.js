import config from '../../assets/js/conf/config';
import {request} from '../../assets/js/utils/request';

//左侧分类
export function getClassifyData(){
    return request(config.baseApi+"/home/category/menu?token="+config.token);
}

//右侧商品(注意：此处有一个小坑，cid是可选的，所以下面路径中不要写固定的死值，要用字符串包裹显示，否则的话，页面点击更新不到新数据)
export function getGoodsData (cid="") {
    return request(config.baseApi+"/home/category/show?cid="+cid+"&token="+config.token);
}

//商品详情
export function getDetailsData(gid=""){
    return request(config.baseApi+"/home/goods/info?gid="+gid+"&type=details&token="+config.token);
}

//获取商品规格
export function getSpecData(gid=""){
    return request(config.baseApi +"/home/goods/info?gid="+gid+"&type=spec&token="+config.token);
}

//加入收藏
export function addFavData(data) {
    return request(config.baseApi+"/goods/fav?uid="+data.uid+"&gid="+data.gid+"&token="+config.token+"");
}