import config from '../../assets/js/conf/config';
import {request} from '../../assets/js/utils/request';

//热门搜索
export function getHotKeywordData () {
    return request(config.baseApi+"/home/public/hotwords?token="+config.token);
}

//搜索商品结果
export function getSearchData (data) {
    let kwords = data.keyword ? data.keyword:"";
    let page = data.page ? data.page : 1;
    let otype = data.otype ? data.otype : "all";
    let cid = data.cid ? data.cid:"";
    let price1 = data.price1 ? data.price1 : "";
    let price2 = data.price2 ? data.price2 : "";
    //判断，如果data.param存在并且data.param不等于空数组，那么执行，否则为空
    let param = data.param && data.param !== '[]' ? data.param : "";
    //因为后期有的接口需要传参，而且逻辑复杂的时候，需要通过参数来进行调试，打印出来看看是否匹配，所以这个地方使用变量进行存储，便于调式
    let url = config.baseApi+"/home/goods/search?kwords="+kwords+"&param="+param+"&page="+page+"&price1="+price1+"&price2="+price2+"&otype="+otype+"&cid="+cid+"&token="+config.token;
    console.log(url)
    return request(url);
}

//商品属性
export function getAttrsData(keyword){
    return request(config.baseApi+"/home/goods/param?kwords="+keyword+"&token="+config.token);
}